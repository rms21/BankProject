<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\AccountType;
use App\LoanType;
use App\EmployeeRole;
use App\Check;
use App\EmployeeCreateCheckLog;
use App\EmployeePayCheckLog;
use App\EmployeeWithdrawDepositAccountLog;
use App\Account;

class CheckController extends Controller
{
    public function postCheckPayment(Request $request)
    {
        $this->validate($request, [
            'check_id' => 'required | numeric | min:1',
            'amount' => 'required | numeric | min: 1',
            'check_number' => 'required | numeric | min:1'
        ]);

        $check = Check::find($request->check_id);
        if (is_null($check)) {
            return back()->withErrors(['check_id' => 'There is no check with this id']);
        }
        if ($check->deleted == 1) {
            return back()->withErrors(['check_id' => 'There is a deleted check']);
        }

        if (!(($request->check_number >= $check->start_number) && ($request->check_number < ($check->start_number + $check->counts)))) {
            return back()->withErrors(['check_number' => 'check number is wrong']);
        }

        $account = Account::find($check->account_id);
        if ($account->balance < $request->amount) {
            return back()->withErrors(['amount' => 'There is not enough account balance']);
        }

        if ($account->deleted == 1) {
            return back()->withErrors(['check_id' => 'The associated account was closed']);
        }

        $account->balance -= $request->amount;
        $account->save();

        $employeeWithdrawDepositLog = new EmployeeWithdrawDepositAccountLog();
        $employeeWithdrawDepositLog->employee_id = Auth::user()->id;
        $employeeWithdrawDepositLog->account_id = $account->id;
        $employeeWithdrawDepositLog->amount = -$request->amount;
        $employeeWithdrawDepositLog->save();

        $employeePaycheckLog = new EmployeePayCheckLog();
        $employeePaycheckLog->employee_id = Auth::user()->id;
        $employeePaycheckLog->check_id = $check->id;
        $employeePaycheckLog->check_number = $request->check_number;
        $employeePaycheckLog->amount = $request->amount;
        $employeePaycheckLog->save();

        return redirect()->route('get_customers_info');
    }

    public function getCheckPayment()
    {
        $employeeRoles = EmployeeRole::where('deleted', '=', '0')->get();
        $user = Auth::user();
        $userRole = EmployeeRole::find($user->employee_role_id);
        $accountTypes = AccountType::where('deleted', '=', '0')->get();
        $loanTypes = LoanType::where('deleted', '=', '0')->get();

        return view('employee.check_payment', ['user' => $user, 'loanTypes' => $loanTypes, 'userRole' => $userRole, 'employeeRoles' => $employeeRoles, 'accountTypes' => $accountTypes ]);
    }

    public function postGiveCheck(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'account_id' => 'required | numeric',
            'counts' => 'required | numeric | min:1'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $account = Account::find($request->account_id);
        if (is_null($account)) {
            return back()->withErrors(['account_id' => 'There is no account with this number']);
        }

        if ($account->deleted == 1) {
            return back()->withErrors(['account_id' => 'This is a deleted account']);
        }

        if ($request->counts < 1) {
            return back()->withErrors(['counts' => 'At least one papaer count should be']);
        }

        //TODO check for account type

        $maxStartNumber = Check::max('start_number');

        $check = new Check();
        $check->account_id = $account->id;
        $check->counts = $request->counts;
        $check->start_number = $maxStartNumber ? $maxStartNumber : 1;
        $check->save();

        $employeeCreateCheckLog = new EmployeeCreateCheckLog();
        $employeeCreateCheckLog->employee_id = Auth::user()->id;
        $employeeCreateCheckLog->check_id = $check->id;
        $employeeCreateCheckLog->save();

        return response()->json('Successfully created', 200);
    }

    public function getCustomersChecksInfo()
    {
        $employeeRoles = EmployeeRole::where('deleted', '=', '0')->get();
        $user = Auth::user();
        $userRole = EmployeeRole::find($user->employee_role_id);
        $accountTypes = AccountType::where('deleted', '=', '0')->get();
        $loanTypes = LoanType::where('deleted', '=', '0')->get();
        $checks = DB::table('customers')
                        ->select('customers.first_name', 'customers.last_name', 'accounts.id as account_id', 'checks.id as check_id', 'checks.counts')
                        ->where('customers.deleted', '=', '0')
                        ->where('accounts.deleted', '=', '0')
                        ->where('checks.deleted', '=', '0')
                        ->join('accounts', 'accounts.customer_id', '=', 'customers.id')
                        ->join('checks', 'checks.account_id', '=', 'accounts.id')
                        ->get();

        return view('employee.customers_checks_info', ['checks' => $checks, 'user' => $user, 'loanTypes' => $loanTypes, 'userRole' => $userRole, 'employeeRoles' => $employeeRoles, 'accountTypes' => $accountTypes ]);
    }
}
