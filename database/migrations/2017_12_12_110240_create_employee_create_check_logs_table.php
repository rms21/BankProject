<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeCreateCheckLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_create_check_logs', function (Blueprint $table) {
            $table->unsignedInteger('employee_id');
            $table->unsignedInteger('check_id');
            $table->timestamps();
        });


        Schema::table('employee_create_check_logs', function ($table) {
          $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
          $table->foreign('check_id')->references('id')->on('checks')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_create_check_logs');
    }
}
