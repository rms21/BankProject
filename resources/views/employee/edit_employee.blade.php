<!DOCTYPE html>
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>ویرایش اطلاعات کارمند</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"
    />
    <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css') }}" rel="stylesheet" type="text/css"
    />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css"
    />
    <link href="{{ asset('assets/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/jqvmap/jqvmap/jqvmap.css') }}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('assets/global/css/components-rtl.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{ asset('assets/global/css/plugins-rtl.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <link href="{{ asset('assets/pages/css/profile-rtl.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{ asset('assets/layouts/layout4/css/layout-rtl.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/layouts/layout4/css/themes/default-rtl.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
    <link href="{{ asset('assets/layouts/layout4/css/custom-rtl.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="stylesheet" href="font.css">
    <script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
</head>

<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
    @include('employee.partials.top-navbar')
    <div class="clearfix"> </div>
    <div class="page-container">
        @include('employee.partials.sidebar')
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-title tabbable-line">
                                            <div class="caption caption-md">
                                                <i class="icon-globe theme-font hide"></i>
                                                <span class="caption-subject font-blue-madison bold uppercase">ویرایش اطلاعات کارمند ها</span>
                                            </div>
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#tab_1_1" data-toggle="tab">تغییر اطلاعات شخصی</a>
                                                </li>
                                                <li>
                                                    <a href="#tab_1_2" data-toggle="tab">تغییر عکس پروفایل</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="tab-content">
                                                <!-- PERSONAL INFO TAB -->
                                                <div class="tab-pane active" id="tab_1_1">
                                                    <form role="form" action="{{ route('post_edit_employee')}}" method="POST">
                                                        <div class="form-group">
                                                            <label class="control-label">نام</label>
                                                            <input type="text" placeholder="{{ $employee->first_name }}" value="{{ old('first_name') }}" class="form-control" name="first_name"/>
                                                            @if($errors->has('first_name'))
                                                              <p class="small text-danger">{{ $errors->get('first_name')[0] }}</p>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">نام خانوادگی</label>
                                                            <input type="text" placeholder="{{ $employee->last_name }}" value="{{ old('last_name')}}" class="form-control" name="last_name"/>
                                                            @if($errors->has('last_name'))
                                                              <p class="small text-danger">{{ $errors->get('last_name')[0] }}</p>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">کد ملی</label>
                                                            <input type="text" placeholder="{{ $employee->national_number }}" value="{{ old('national_number') }}" class="form-control" name="national_number"/>
                                                            @if($errors->has('national_number'))
                                                              <p class="small text-danger">{{ $errors->get('national_number')[0] }}</p>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">نقش</label>
                                                            <!-- <input type="text" value="" class="form-control" /> -->
                                                            <select class="bs-select form-control input" name="employee_role_id">
                                                                @foreach ($employeeRoles as $role)
                                                                  <option value="{{ $role->id }}" {{ $role->id == $employee->employee_role_id ? 'selected=selected' : ''}}>{{ $role->role }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label">ملیت</label>
                                                            <input type="text" placeholder="{{ $employee->nationality }}" value="{{ old('nationality') }}" class="form-control" name="nationality"/>
                                                            @if($errors->has('nationality'))
                                                              <p class="small text-danger">{{ $errors->get('nationality')[0] }}</p>
                                                            @endif
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label">جنسیت</label>
                                                            <select class="bs-select form-control input" title="جنسیت" name="gender">
                                                                <option value="male" {{ $employee->gender=="male" ? 'selected=selected' : ''}}>مذکر</option>
                                                                <option value="female" {{ $employee->gender=="female" ? 'selected=selected' : ''}}>مونث</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">ادرس</label>
                                                            <input type="text" placeholder="{{ $employee->address }}" value="{{ old('address') }}" class="form-control" name="address"/>
                                                            @if($errors->has('address'))
                                                              <p class="small text-danger">{{ $errors->get('address')[0] }}</p>
                                                            @endif
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label">ایمیل</label>
                                                            <input type="text" placeholder="{{ $employee->email }}" value="{{ old('email') }}" class="form-control" name="email"/>
                                                            @if($errors->has('email'))
                                                              <p class="small text-danger">{{ $errors->get('email')[0] }}</p>
                                                            @endif
                                                        </div>
                                                        <div class="margin-top-10" style="float: left !important">
                                                            <button type="submit" class="btn green">ذخیره تغییرات</button>
                                                            <button type="button" class="btn defult">لغو</button>
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            <input type="hidden" name="id" value="{{ $employee->id }}">
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </form>
                                                </div>
                                                <!-- END PERSONAL INFO TAB -->
                                                <!-- CHANGE AVATAR TAB -->
                                                <div class="tab-pane" id="tab_1_2">
                                                    <form action="#" role="form">
                                                        <div class="form-group">
                                                            <div class=" fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                <div>
                                                                    <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> انتخاب تصویر </span>
                                                                        <span class="fileinput-exists"> تغییر </span>
                                                                        <input type="file" name="..."> </span>
                                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> حذف </a>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="margin-top-10">
                                                            <button type="submit" class="btn green">ذخیره تغییرات</button>
                                                            <button type="button" class="btn defult">لغو</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN CORE PLUGINS -->
    <script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/flot/jquery.flot.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/profile.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="{{ asset('assets/layouts/layout4/scripts/layout.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/layouts/layout4/scripts/demo.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>
