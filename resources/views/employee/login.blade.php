<!DOCTYPE html>
<html dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <link rel="stylesheet" href="{{ URL::to('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/material-kit.css') }}">

    <link rel="stylesheet" href="{{ URL::to('assets/css/employee-login.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/helpers.css') }}">
    <script src="{{ URL::to('assets/js/jquery.min.js') }}"></script>

    <title>Login</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row bg-dark">
            <div class="col-sm-4 col-sm-offset-4" align="center">
                <img src="{{ URL::to('assets/img/bg_logo.png') }}" alt="">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 middle shadow-depth-1">
                <div class="col-sm-6">
                    <h4 class="text-danger" align="center">نکات امنیتی</h4>
                    <hr class="style-two">
                    <ul class="s-icon">
                        <li> از صحت URL وارد شده درصفحه مرورگر خود اطمینان حاصل فرمایید. </li>
                        <li>رمز عبور خود را در فواصل زمانی کوتاه تغییر داده و هرگز آن را به سایرین ندهید.</li>
                        <li>همواره از رايانه‌ امن مجهز به ضدویروس به روز شده و دارای آخرین وصله‌های امنیتی استفاده نمایید.</li>
                        <li>بانک هرگز اطلاعات محرمانه شما را از طریق ایمیل یا پیامک درخواست نمی کند. در صورت رخداد این مورد، ضمن عدم پاسخگویی به چنین
                        درخواستهایی مراتب را سریعاً به بانک اطلاع دهید.</li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <h4 align="center">ورود به سامانه</h4>
                    <hr class="style-two">
                    <form class="col-sm-10 col-sm-offset-1" action="{{ route('post_employee_login') }}" method="POST">
                        <div class="input-group">
    						<span class="input-group-addon">
    							<i class="material-icons">email</i>
    						</span>
    						<input type="email" class="form-control" placeholder="ایمیل" name="email">
                            @if($errors->has('email'))
                              <p class="small text-danger">{{ $errors->get('email')[0] }}</p>
                            @endif
						</div>
                        <div class="input-group">
    						<span class="input-group-addon">
    							<i class="material-icons">lock</i>
    						</span>
    						<input type="password" class="form-control" placeholder="رمز عبور" name="password">
                            @if($errors->has('password'))
                                <p class="small text-danger" >{{ $errors->get('password')[0] }}</p>
                            @endif
						</div>
                        <div class="form-group is-text-left">
                            <button type="submit" class="btn btn-info" name="submit">ورود</button>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script src="{{ URL::to('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/material.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/material-kit.js') }}"></script>
</body>
</html>
