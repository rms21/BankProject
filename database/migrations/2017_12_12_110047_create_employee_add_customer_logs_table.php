<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeAddCustomerLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_add_customer_logs', function (Blueprint $table) {
            $table->unsignedInteger('employee_id');
            $table->unsignedInteger('customer_id');
            $table->timestamps();
        });

        Schema::table('employee_add_customer_logs', function ($table) {
          $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
          $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_add_customer_logs');
    }
}
