<?php

use Illuminate\Database\Seeder;

use App\Employee;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employee = new Employee();
        $employee->employee_role_id = 1;
        $employee->first_name = "rasoul";
        $employee->last_name = "mohamad salehi";
        $employee->nationality = "iranian";
        $employee->email = "rasoul_salehi@ymail.com";
        $employee->national_number = "4060893293";
        $employee->address = "lorestan";
        $employee->gender = "male";
        $employee->password = bcrypt("rasoul21");
        $employee->profile_image_path = "uploads/img/profileImage/1.jpg";
        $employee->national_card_image_path = "uploads/img/nationalCardImage/1.jpg";
        $employee->identity_certificate_image_path = "uploads/img/identityCertificateImage/1.jpg";
        $employee->save();
    }
}
