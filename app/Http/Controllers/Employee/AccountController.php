<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\AccountType;
use App\EmployeeRole;
use App\LoanType;
use App\Account;
use App\Customer;
use App\EmployeeOpenAccountLog;
use App\EmployeeDeleteAccountLog;

class AccountController extends Controller
{
    public function postDeleteAccount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'account_id' => 'required | numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $account = Account::find($request->account_id);
        if (is_null($account)) {
            return response()->json(['fail' => 'There is no account with this number'], 400);
        }

        $account->deleted = 1;
        $account->save();

        $employeeDeleteAccountLog = new EmployeeDeleteAccountLog();
        $employeeDeleteAccountLog->employee_id = Auth::user()->id;
        $employeeDeleteAccountLog->account_id = $account->id;
        $employeeDeleteAccountLog->save();

        return response()->json("account successfully deleted", 200);
    }

    public function getAddAccount()
    {
        $employeeRoles = EmployeeRole::where('deleted', '=', '0')->get();
        $user = Auth::user();
        $userRole = EmployeeRole::find($user->employee_role_id);
        $accountTypes = AccountType::where('deleted', '=', '0')->get();
        $loanTypes = LoanType::where('deleted', '=', '0')->get();

        return view('employee.add_account', ['user' => $user, 'loanTypes'=> $loanTypes, 'userRole' => $userRole, 'employeeRoles' => $employeeRoles, 'accountTypes' => $accountTypes ]);
    }

    public function postAddAccount(Request $request)
    {
        $this->validate($request, [
            'national_number' => 'required | min: 10 | max: 10',
            'balance' => 'required | numeric',
            'account_type_id' => 'required'
        ]);

        $customer = Customer::where('national_number', '=', $request->national_number)->first();
        if (!is_null($customer)) {
            $account = new Account();
            $account->account_type_id = $request->account_type_id;
            $account->customer_id = $customer->id;
            $account->balance = $request->balance;
            $account->save();

            $employeeOpenAccountLog = new EmployeeOpenAccountLog();
            $employeeOpenAccountLog->employee_id = Auth::user()->id;
            $employeeOpenAccountLog->account_id = $account->id;

            return redirect()->route('get_customers_info');
        }

        return back()->withErrors(['fail' => 'cusomter does not exist']);
    }

    public function postEditAccountType(Request $request)
    {
        if (!EmployeeController::isAdmin()) {
            return back();
        }

        $validator = Validator::make($request->all(), [
        'accountTypeId' => 'required',
        'accountTypeInterestRate' => [ 'required', 'numeric']
      ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $accountType = AccountType::find($request->accountTypeId);
        $accountType->interest_rate = $request->accountTypeInterestRate;
        $accountType->save();

        return response()->json("updated successfully", 200);
    }
    public function postAddAccountType(Request $request)
    {
        if (!EmployeeController::isAdmin()) {
            return back();
        }

        $validator = Validator::make($request->all(), [
        'accountTypeName' => [ 'required', 'max:250', 'unique:account_types,name' ],
        'accountTypeInterestRate' => [ 'required', 'numeric']
      ]);

        if ($validator->messages()->has('accountTypeName') && $validator->messages()->get('accountTypeName')[0] == "The account type name has already been taken."
      && !$validator->messages()->has('accountTypeInterestRate')) {
            $accountType = AccountType::where('name', '=', $request->accountTypeName)->first();
            if ($accountType->deleted == 1) {
                $accountType->deleted = 0;
                $accountType->interest_rate = $request->accountTypeInterestRate;
                $accountType->save();
            } else {
                return response()->json($validator->messages(), 400);
            }
        } elseif ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        } else {
            $accountType = new AccountType();
            $accountType->name = $request->accountTypeName;
            $accountType->interest_rate = $request->accountTypeInterestRate;
            $accountType->save();
        }

        return response()->json(['accountTypeId' => $accountType->id], 200);
    }

    public function postDeleteAccountType(Request $request)
    {
        if (!EmployeeController::isAdmin()) {
            return back();
        }

        $validator = Validator::make($request->all(), [
            'accountTypeId' => 'required'
        ]);

        $accountType = AccountType::find($request->accountTypeId);
        $accountType->deleted = 1;
        $accountType->save();

        return response()->json("deleted successfully", 200);
    }
}
