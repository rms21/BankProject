<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\EmployeeRole;
use App\AccountType;
use App\LoanType;

class DashboardController extends Controller
{
    public function getDashboard()
    {
        $user = Auth::user();
        $role = EmployeeRole::find($user->employee_role_id);
        $employeeRoles = EmployeeRole::where('deleted', '=', '0')->get();
        $accountTypes = AccountType::where('deleted', '=', '0')->get();
        $loanTypes = LoanType::where('deleted', '=', '0')->get();

        return view('employee.dashboard', ['user' => $user, 'loanTypes' => $loanTypes, 'userRole' => $role, 'employeeRoles' => $employeeRoles, 'accountTypes' => $accountTypes ]);
    }
}
