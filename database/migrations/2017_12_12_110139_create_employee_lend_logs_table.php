<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeLendLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_lend_logs', function (Blueprint $table) {
            $table->unsignedInteger('employee_id');
            $table->unsignedInteger('loan_id');
            $table->timestamps();
        });

      Schema::table('employee_lend_logs', function ($table) {
          $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
          $table->foreign('loan_id')->references('id')->on('loans')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_lend_logs');
    }
}
