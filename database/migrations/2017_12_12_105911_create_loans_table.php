<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('account_id');
            $table->unsignedInteger('loan_type_id');
            $table->double('payed_amount')->default('0');
            $table->boolean('deleted')->default(false);
            $table->timestamps();
        });

        Schema::table('loans', function ($table) {
          $table->foreign('loan_type_id')->references('id')->on('loan_types')->onDelete('cascade');
          $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
