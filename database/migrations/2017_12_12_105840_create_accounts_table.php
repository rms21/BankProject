<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('account_type_id');
            $table->unsignedInteger('customer_id');
            $table->double('balance');
            $table->double('interest')->default('0');
            $table->boolean('deleted')->default(false);
            $table->timestamps();
        });

        Schema::table('accounts', function ($table) {
          $table->foreign('account_type_id')->references('id')->on('account_types')->onDelete('cascade');
          $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
