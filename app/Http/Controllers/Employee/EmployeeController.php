<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\EmployeeRole;
use App\Employee;
use App\AccountType;
use App\LoanType;

class EmployeeController extends Controller
{
    public function postEditEmployee(Request $request)
    {
        if (!EmployeeController::isAdmin()) {
            return back();
        }

        $this->validate($request, [
            'first_name' => 'required | max: 255',
            'last_name' => 'required | max: 255',
            'nationality' => 'required | max: 255',
            'national_number' => 'required | min: 10 | max: 10',
            'email' => 'required | max: 255',
            'address' => 'required | max: 255',
            'gender' => 'required | alpha | max:50',
            'employee_role_id' => 'required | numeric',
    ]);

        $employee = Employee::find($request->id);
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->nationality = $request->nationality;
        $employee->national_number = $request->national_number;
        $employee->email = $request->email;
        $employee->address = $request->address;
        $employee->gender = $request->gender;
        $employee->employee_role_id = $request->employee_role_id;
        $employee->save();

        return redirect()->route('get_employees_info');
    }

    public function getEmployeesInfo()
    {
        if (!EmployeeController::isAdmin()) {
            return back();
        }

        $employeeRoles = EmployeeRole::where('deleted', '=', '0')->get();
        $user = Auth::user();
        $userRole = EmployeeRole::find($user->employee_role_id);
        $employees = DB::table('employees')->join('employee_roles', 'employees.employee_role_id', '=', 'employee_roles.id')->where('employees.deleted', '=', '0')->select('employees.*', 'employee_roles.role')->get();
        $accountTypes = AccountType::where('deleted', '=', '0')->get();
        $loanTypes = LoanType::where('deleted', '=', '0')->get();

        return view('employee.employees_info', ['user' => $user, 'loanTypes' => $loanTypes,  'userRole' => $userRole, 'employees' => $employees, 'employeeRoles' => $employeeRoles, 'accountTypes' => $accountTypes ]);
    }


    public function getEditEmployee($id)
    {
        $employeeRoles = EmployeeRole::where('deleted', '=', '0')->get();
        $user = Auth::user();
        $userRole = EmployeeRole::find($user->employee_role_id);
        $accountTypes = AccountType::where('deleted', '=', '0')->get();
        $employee = Employee::find($id);
        $loanTypes = LoanType::where('deleted', '=', '0')->get();

        return view('employee.edit_employee', ['employee' => $employee, 'loanTypes' => $loanTypes, 'user' => $user, 'userRole' => $userRole, 'employeeRoles' => $employeeRoles, 'accountTypes' => $accountTypes ]);
    }

    public function getDeleteEmployee($id)
    {
        if (!EmployeeController::isAdmin()) {
            return back();
        }

        $employee = Employee::find($id);
        $employee->deleted="1";
        $employee->save();

        return redirect()->route('get_employees_info');
    }

    public function postAddEmployeeRole(Request $request)
    {
        if (!EmployeeController::isAdmin()) {
        }

        $validator = Validator::make($request->all(), [
          'role' => [ 'required', 'unique:employee_roles', 'max:250']
        ]);

        if ($validator->messages()->has("role") && $validator->messages()->get("role")[0] == "The role has already been taken.") {
            $employeeRole = EmployeeRole::where('role', '=', $request->role)->first();
            if ($employeeRole->deleted == 1) {
                $employeeRole->deleted = 0;
                $employeeRole->save();
            } else {
                return response()->json($validator->messages(), 400);
            }
        } elseif ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        } else {
            $employeeRole = new EmployeeRole();
            $employeeRole->role = $request->role;
            $employeeRole->save();
        }
        return response()->json(['roleId' => $employeeRole->id], 200);
    }

    public function postDeleteEmployeeRole(Request $request)
    {
        if (!EmployeeController::isAdmin()) {
            return back();
        }

        $validator = Validator::make($request->all(), [
            'roleId' => 'required'
        ]);

        $role = EmployeeRole::find($request->roleId);
        $role->deleted = 1;
        $role->save();

        return response()->json("deleted successfully", 200);
    }

    public static function isAdmin()
    {
        $user = Auth::user();
        $role = EmployeeRole::find($user->employee_role_id);
        if ($role->role != "admin") {
            return false;
        }
        return true;
    }
}
