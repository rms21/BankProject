<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('employee')->group(function () {
    Route::get('login', 'LoginRegisterController@getLogin')->name('get_employee_login');
    Route::post('login', 'LoginRegisterController@postLogin')->name('post_employee_login');
    Route::get('logout', 'LoginRegisterController@getLogout')->name('get_employee_logout');


    Route::group(['middleware' => ['auth']], function () {
        Route::get('register', 'LoginRegisterController@getRegister')->name('get_employee_register');
        Route::post('register', 'LoginRegisterController@postRegister')->name('post_employee_register');

        Route::prefix('dashboard')->group(function () {
            Route::get('', 'DashboardController@getDashboard')->name('get_employee_dashboard');
            Route::get('employees/info', "Employee\EmployeeController@getEmployeesInfo")->name('get_employees_info');
            Route::get('delete/employee/{id}', "Employee\EmployeeController@getDeleteEmployee")->name('get_delete_employee');
            Route::post('add/employee/role', "Employee\EmployeeController@postAddEmployeeRole")->name('post_add_employee_role');
            Route::post('delete/employee/role', "Employee\EmployeeController@postDeleteEmployeeRole")->name('post_delete_employee_role');
            Route::post('add/account/type', "Employee\AccountController@postAddAccountType")->name('post_add_account_type');
            Route::post('delete/account/type', "Employee\AccountController@postDeleteAccountType")->name('post_delete_account_type');
            Route::post('edit/account/type', "Employee\AccountController@postEditAccountType")->name('post_edit_account_type');
            Route::get('edit/employee/{id}', "Employee\EmployeeController@getEditEmployee")->name('get_edit_employee');
            Route::post('edit/employee', "Employee\EmployeeController@postEditEmployee")->name('post_edit_employee');
            Route::get('add/account', "Employee\AccountController@getAddAccount")->name('get_add_account');
            Route::post('add/account', "Employee\AccountController@postAddAccount")->name('post_add_account');
            Route::post('delete/account', "Employee\AccountController@postDeleteAccount")->name('post_delete_account');
            Route::get('add/customer', "Employee\CustomerController@getAddCustomer")->name('get_add_customer');
            Route::post('add/customer', "Employee\CustomerController@postAddCustomer")->name('post_add_customer');
            Route::get('check/payment', "Employee\CheckController@getCheckPayment")->name('get_check_payment');
            Route::post('check/payment', "Employee\CheckController@postCheckPayment")->name('post_check_payment');
            Route::post('give/check', "Employee\CheckController@postGiveCheck")->name('post_give_loan');
            Route::get('customers/checks/info', "Employee\CheckController@getCustomersChecksInfo")->name('get_customers_checks_info');
            Route::get('customers/info', "Employee\CustomerController@getCustomersInfo")->name('get_customers_info');
            Route::get('give/loan', "Employee\LoanController@getGiveLoan")->name('get_give_loan');
            Route::post('give/loan', "Employee\LoanController@postGiveLoan")->name('post_give_loan');
            Route::get('pay/loan', "Employee\LoanController@getPayLoan")->name('get_pay_loan');
            Route::post('pay/loan', "Employee\LoanController@postPayLoan")->name('post_pay_loan');
            Route::post('add/loan/type', "Employee\LoanController@postAddLoanType")->name('post_add_loan_type');
            Route::post('delete/loan/type', "Employee\LoanController@postDeleteLoanType")->name('post_delete_loan_type');
            Route::post('edit/loan/type', "Employee\LoanController@postEditLoanType")->name('post_edit_loan_type');
            Route::post('deposit/to/customer/account', "Employee\CustomerController@postDepositToCustomerAccount")->name('post_deposit_to_customer_account');
            Route::post('withdraw/from/customer/account', "Employee\CustomerController@postWithdrawFromCustomerAccount")->name('post_withdraw_from_customer_account');
            Route::get('customers/loans/info', "Employee\LoanController@getCustomersLoansInfo")->name('get_customers_loans_info');
        });
    });
});
