<?php

use Illuminate\Database\Seeder;

use App\EmployeeRole;

class EmployeeRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employeeRole = new EmployeeRole();
        $employeeRole->role = "employee";
        $employeeRole->save();
    }
}
