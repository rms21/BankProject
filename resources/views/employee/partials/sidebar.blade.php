<script src="{{ asset('assets/js/modals.js') }}"></script>
<div class="page-sidebar-wrapper">
  <div class="page-sidebar navbar-collapse collapse">
    <div class="portlet light profile-sidebar-portlet bordered ">
      <!-- SIDEBAR USERPIC -->
      <div class="profile-userpic">
        <img src="{{ asset($user->profile_image_path) }}" class="img-responsive" alt=""> </div>
      <!-- END SIDEBAR USERPIC -->
      <!-- SIDEBAR USER TITLE -->
      <div class="profile-usertitle">
        <div class="profile-usertitle-name">{{ $user->first_name . ' ' . $user->last_name }}</div>
        <div class="profile-usertitle-job">{{ $userRole->role }}</div>
      </div>
      <div class="profile-usermenu">
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
          {{--
          <li class="nav-item ">
            <a href="page_user_profile_1.html" class="nav-toggle"><i class="icon-home"></i><span class="title">صفحه کاربری</span></a>
          </li> --}}
          <li class="nav-item">
            <a href="{{ route('get_employee_logout') }}"><i class="icon-logout"></i><span class="title">خروج</span></a>
          </li>
        </ul>
      </div>
    </div>
    <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
      @if ($userRole->role == "admin")
      <li class="nav-item start active open">
        <a href="javascript:;" class="nav-link nav-toggle"><i class="icon-user"></i><span class="title">کارمند ها</span><span class="selected"></span><span class="arrow "></span></a>
        <ul class="sub-menu">
          <li class="nav-item start active">
            <a href="{{ route('get_employees_info') }}" class="nav-link"><span class="title">مشاهده کارمند ها</span></a>
          </li>
          <li class="nav-item start">
            <a href="#add-role" class="nav-link" data-toggle="modal"><span class="title">اضافه کردن نقش کارمند</span></a>
            <div class="modal fade" id="add-role" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title text-danger">اضافه کردن نقش کارمند</h4>
                  </div>
                  <div class="modal-body">
                    <div class="portlet-body form">
                      <form role="form" method="POST" id="addEmployeeRoleForm">
                        <div class="form-body">
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="نام نقش" name="role">
                            <span class="help-block"></span>
                            <p class="small text-danger" id="addEmployeeRoleFormRoleError"></p>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn dark btn-outline" data-dismiss="modal">لغو</button>
                          <button type="submit" class="btn green">ذخیره تغییرات</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="nav-item start ">
            <a href="#delete-role" class="nav-link " data-toggle="modal">
                                          <span class="title">حذف نقش کارمند</span>
                                      </a>
            <div class="modal fade" id="delete-role" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title text-danger">حذف نقش کارمند</h4>
                  </div>
                  <div class="modal-body">
                    <div class="portlet-body form">
                      <form role="form" method="POST" id="deleteEmployeeRoleForm">
                        <div class="form-body">
                          <div class="form-group form-md-line-input">
                            <select class="bs-select form-control input" name="roleId">
                                @foreach ($employeeRoles as $role)
                                  <option value="{{ $role->id }}">{{ $role->role }}</option>
                                @endforeach
                              </select>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn dark btn-outline" data-dismiss="modal">لغو</button>
                          <button type="submit" class="btn green">ذخیره تغییرات</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </li>
      @endif

      <li class="nav-item  ">
        <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">مشتری ها</span>
                                <span class="arrow"></span>
                            </a>
        <ul class="sub-menu">

          <li class="nav-item  ">
            <a href="{{ route('get_add_customer') }}" class="nav-link ">
                                        <span class="title">اضافه کردن مشتری</span>
                                    </a>
          </li>
          <li class="nav-item  ">
            <a href="#add-money" class="nav-link " data-toggle="modal">
              <span class="title">واریز به حساب مشتری</span>
            </a>
            <div class="modal fade" id="add-money" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title text-danger">واریز به حساب</h4>
                  </div>
                  <div class="modal-body">
                    <div class="portlet-body form">
                      <form role="form" method="POST" id="depositToCustomerAccountForm">
                        <div class="form-body">
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="شماره حساب" name="account_id">
                            <p class="small text-danger" id="depositToCustomerAccountFormAccountIdError"></p>
                          </div>
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="مبلغ واریزی" name="amount">
                            <p class="small text-danger" id="depositToCustomerAccountFormAmountError"></p>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn dark btn-outline" data-dismiss="modal">لغو</button>
                          <button type="submit" class="btn green">ذخیره تراکنش</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="nav-item  ">
            <a href="#draw" class="nav-link " data-toggle="modal">
              <span class="title">برداشت از حساب مشتری</span>
            </a>
            <div class="modal fade" id="draw" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title text-danger">برداشت از حساب</h4>
                  </div>
                  <div class="modal-body">
                    <div class="portlet-body form">
                      <form role="form" method="POST" id="withdrawFromCustomerAccountForm">
                        <div class="form-body">
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="شماره حساب" name="account_id">
                            <p class="small text-danger" id="withdrawFromCustomerAccountFormAccountIdError"></p>
                          </div>
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="مبلغ برداشت" name="amount">
                            <p class="small text-danger" id="withdrawFromCustomerAccountFormAmountError"></p>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn dark btn-outline" data-dismiss="modal">لغو</button>
                          <button type="submit" class="btn green">ذخیره تراکنش</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="nav-item  ">
            <a href="{{ route('get_customers_info') }}" class="nav-link ">
              <span class="title">مشاهده مشتریان</span>
            </a>
          </li>
        </ul>
      </li>

      <li class="nav-item  ">
        <a href="?p=" class="nav-link nav-toggle">
                                <i class="icon-wallet"></i>
                                <span class="title">مدیریت حساب ها </span>
                                <span class="arrow"></span>
                            </a>
        <ul class="sub-menu">
          <li class="nav-item start active ">
            <a href="{{ route('get_add_account') }}" class="nav-link ">
                                        <span class="title">اضافه کردن حساب</span>
                                    </a>
          </li>
          {{-- <li class="nav-item ">
            <a href="#ban_acount" class="nav-link " data-toggle="modal">
                                        <span class="title">مسدود کردن حساب</span>
                                    </a>
            <div class="modal fade" id="ban_acount" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title text-danger">مسدود کردن حساب</h4>
                  </div>
                  <div class="modal-body">
                    <div class="portlet-body form">
                      <form role="form" method="POST">
                        <div class="form-body">
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="شماره حساب">
                            <span class="help-block">شماره حساب مشتری</span>
                          </div>
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="دلیل مسدود کردن حساب">
                            <span class="help-block">دلیل مسدود کردن حساب</span>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn dark btn-outline" data-dismiss="modal">لغو</button>
                          <button type="button" class="btn green">ذخیره تغییرات</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li> --}}
          <li class="nav-item  ">
            <a href="#delete_acount" class="nav-link " data-toggle="modal">
                <span class="title">حذف کردن حساب</span>
            </a>
            <div class="modal fade" id="delete_acount" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title text-danger">حذف کردن حساب</h4>
                  </div>
                  <div class="modal-body">
                    <div class="portlet-body form">
                      <form role="form" method="POST" id="deleteAccountForm">
                        <div class="form-body">
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="شماره حساب" name="account_id">
                            <p class="small text-danger" id="deleteAccountFormError"></p>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn dark btn-outline" data-dismiss="modal">لغو</button>
                          <button type="submit" class="btn green">ذخیره تغییرات</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          @if($userRole->role == "admin")
            <li class="nav-item  ">
              <a href="#add_acount_type" class="nav-link " data-toggle="modal">
                  <span class="title">اضافه کردن نوع حساب</span>
                </a>
              <div class="modal fade" id="add_acount_type" tabindex="-1" role="basic" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h4 class="modal-title text-danger">اضافه کردن نوع حساب</h4>
                    </div>
                    <div class="modal-body">
                      <div class="portlet-body form">
                        <form role="form" method="POST" id="addAccountTypeForm">
                          <div class="form-body">
                            <div class="form-group form-md-line-input">
                              <input type="text" class="form-control" id="form_control_1" placeholder="نوع حساب" name="accountTypeName">
                              <p class="small text-danger" id="addAccountTypeFormNameError"></p>
                            </div>
                            <div class="form-group form-md-line-input">
                              <input type="text" class="form-control" id="form_control_1" placeholder="سود اختصاصی" name="accountTypeInterestRate">
                              <p class="small text-danger" id="addAccountTypeFormInterestError"></p>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">لغو</button>
                            <button type="submit" class="btn green">ذخیره تغییرات</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li class="nav-item  ">
              <a href="#edit_acount_type" class="nav-link " data-toggle="modal">
                                            <span class="title">ویرایش نوع حساب</span>
                                        </a>
              <div class="modal fade" id="edit_acount_type" tabindex="-1" role="basic" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h4 class="modal-title text-danger">ویرایش نوع حساب</h4>
                    </div>
                    <div class="modal-body">
                      <div class="portlet-body form">
                        <form role="form" method="POST" id="editAccountTypeForm">
                          <div class="form-body">
                            <div class="form-group form-md-line-input">
                              <select class="bs-select form-control input" name="accountTypeId">
                                  @foreach ($accountTypes as $accountType)
                                    <option value="{{ $accountType->id }}">{{ $accountType->name }}</option>
                                  @endforeach
                                </select>
                            </div>
                            <div class="form-group form-md-line-input">
                              <input type="text" class="form-control" id="form_control_1" placeholder="سود جدید" name="accountTypeInterestRate">
                              <p class="small text-danger" id="editAccountTypeFormInterestError"></p>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">لغو</button>
                            <button type="submit" class="btn green">ذخیره تغییرات</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li class="nav-item  ">
            <a href="#delete_acount_type" class="nav-link " data-toggle="modal">
                  <span class="title">حذف کردن نوع حساب</span>
              </a>
            <div class="modal fade" id="delete_acount_type" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title text-danger">حذف نوع حساب</h4>
                  </div>
                  <div class="modal-body">
                    <div class="portlet-body form">
                      <form role="form" method="POST" id="deleteAccountTypeForm">
                        <div class="form-body">
                          <div class="form-group form-md-line-input">
                            <select class="bs-select form-control input" name="accountTypeId">
                                @foreach ($accountTypes as $accountType)
                                  <option value="{{ $accountType->id }}">{{ $accountType->name }}</option>
                                @endforeach
                              </select>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn dark btn-outline" data-dismiss="modal">لغو</button>
                          <button type="submit" class="btn green">ذخیره تغییرات</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          @endif
        </ul>
      </li>
      <li class="nav-item">
        <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-money"></i>
                                <span class="title">وام</span>
                                <span class="arrow"></span>
                            </a>
        <ul class="sub-menu">
          <li class="nav-item start active ">
            <a href="{{ route('get_customers_loans_info') }}" class="nav-link ">
              <span class="title">وام ها</span>
            </a>
          </li>
          <li class="nav-item start">


            <a href="{{ route('get_pay_loan') }}" class="nav-link ">
                <span class="title">بازپرداخت وام</span>
            </a>
          </li>
          <li class="nav-item  ">
            <a href="{{ route('get_give_loan') }}" class="nav-link ">
                                        <span class="title">تخصیص وام </span>
                                    </a>
          </li>
          @if($userRole->role == "admin")
          <li class="nav-item  ">
            <a href="#add_loan_type" class="nav-link " data-toggle="modal">
                                        <span class="title">اضافه کردن نوع وام</span>
                                    </a>
            <div class="modal fade" id="add_loan_type" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title text-danger">اضافه کردن نوع وام</h4>
                  </div>
                  <div class="modal-body">
                    <div class="portlet-body form">
                      <form role="form" method="POST" id="addLoanTypeForm">
                        <div class="form-body">
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="نام نوع جدید وام" name="loanTypeName">
                            <p class="small text-danger" id="addLoanTypeFormNameError"></p>
                          </div>
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="مبلغ وام" name="amount">
                            <p class="small text-danger" id="addLoanTypeFormAmountError"></p>
                          </div>
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="سود نوع وام" name="interest">
                            <p class="small text-danger" id="addLoanTypeFormInterestError"></p>
                          </div>
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="تعداد اقساط" name="number_of_payments">
                            <p class="small text-danger" id="addLoanTypeFormNumberOfPaymentsError"></p>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn dark btn-outline" data-dismiss="modal">لغو</button>
                          <button type="submit" class="btn green">ذخیره تغییرات</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="nav-item  ">
            <a href="#delete_loan_type" class="nav-link " data-toggle="modal">
                <span class="title">حذف نوع وام</span>
            </a>
            <div class="modal fade" id="delete_loan_type" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title text-danger">حذف نوع وام</h4>
                  </div>
                  <div class="modal-body">
                    <div class="portlet-body form">
                      <form role="form" method="POST" id="deleteLoanTypeForm">
                        <div class="form-body">
                          <div class="form-group form-md-line-input">
                            <select class="bs-select form-control input" name="loanTypeId">
                              @foreach ($loanTypes as $loanType)
                                <option value="{{ $loanType->id }}"> {{ $loanType->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn dark btn-outline" data-dismiss="modal">لغو</button>
                          <button type="submit" class="btn green">ذخیره تغییرات</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="nav-item  ">
            <a href="#edit_loan_type" class="nav-link " data-toggle="modal">
                                        <span class="title">ویرایش نوع وام</span>
                                    </a>
            <div class="modal fade" id="edit_loan_type" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title text-danger">ویرایش نوع وام</h4>
                  </div>
                  <div class="modal-body">
                    <div class="portlet-body form">
                      <form role="form" method="POST" id="editLoanTypeForm">
                        <div class="form-body">
                          <div class="form-group form-md-line-input">
                            <select class="bs-select form-control input" name="loanTypeId">
                              @foreach ($loanTypes as $loanType)
                                <option value="{{ $loanType->id }}"> {{ $loanType->name }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="مبلغ وام" name="amount">
                            <p class="small text-danger" id="editLoanTypeFormAmountError"></p>
                          </div>
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="سود نوع وام" name="interest">
                            <p class="small text-danger" id="editLoanTypeFormInterestError"></p>
                          </div>
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="تعداد اقساط" name="number_of_payments">
                            <p class="small text-danger" id="editLoanTypeFormNumberOfPaymentsError"></p>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn dark btn-outline" data-dismiss="modal">لغو</button>
                          <button type="submit" class="btn green">ذخیره تغییرات</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          @endif
        </ul>
      </li>
      <li class="nav-item  ">
        <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-credit-card"></i>
                                <span class="title">چک </span>
                                <span class="arrow"></span>
                            </a>
        <ul class="sub-menu">
          <li class="nav-item start ">
            <a href="{{ route('get_customers_checks_info') }}" class="nav-link ">
              <span class="title">چک ها</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('get_check_payment') }}" class="nav-link ">
                                        <span class="title">پرداخت چک</span>
                                    </a>
          </li>
          <li class="nav-item  ">
            <a href="#assign_check" class="nav-link" data-toggle="modal">
                                        <span class="title">تخصیص چک</span>
                                    </a>
            <div class="modal fade" id="assign_check" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title text-danger">تخصیص چک</h4>
                  </div>
                  <div class="modal-body">
                    <div class="portlet-body form">
                      <form role="form" method="POST" id="giveCheckForm">
                        <div class="form-body">
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="شماره حساب" name="account_id">
                            <p class="small text-danger" id="giveCheckFormAccountIdError"></p>
                          </div>
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="تعداد" name="counts">
                            <p class="small text-danger" id="giveCheckFormCountsError"></p>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn dark btn-outline" data-dismiss="modal">لغو</button>
                          <button type="submit" class="btn green">ذخیره تغییرات</button>
                        </div>
                      </form>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </li>

          <li class="nav-item  ">
            <a href="#ban-check" class="nav-link" data-toggle="modal">
                                        <span class="title">مسدود کردن چک</span>
                                    </a>
            <div class="modal fade" id="ban-check" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title text-danger">مسدود کردن چک</h4>
                  </div>
                  <div class="modal-body">
                    <div class="portlet-body form">
                      <form role="form" method="POST">
                        <div class="form-body">
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="شماره حساب چک">
                            <span class="help-block">شماره حساب چک</span>
                          </div>
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="دلیل مسدود کردن چک">
                            <span class="help-block">باید دلیل داشته باشه</span>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn dark btn-outline" data-dismiss="modal">لغو</button>
                          <button type="submit" class="btn green">ذخیره تغییرات</button>
                        </div>
                      </form>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</div>
