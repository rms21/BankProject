<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employee_role_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('nationality');
            $table->string('email');
            $table->string('national_number');
            $table->string('address');
            $table->string('gender', 60);
            $table->string('password');
            $table->string('profile_image_path');
            $table->string('national_card_image_path');
            $table->string('identity_certificate_image_path');
            $table->boolean('deleted')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::table('employees', function ($table) {
          $table->foreign('employee_role_id')->references('id')->on('employee_roles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
