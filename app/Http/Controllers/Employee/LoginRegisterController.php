<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Employee;
use App\EmployeeRole;

class LoginRegisterController extends Controller
{
    public function getLogin()
    {
        //check if user already logged in
        if(Auth::check()){
          return redirect()->route('get_employee_dashboard');
        }
        return view('employee.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
      'email' => 'required | email | max: 255',
      'password' => 'required | min:8 | max: 255'
    ]);


        if (!Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->route('get_employee_login');
        }

        //if user was deleted
        if(Auth::user()->deleted == "1"){
            Auth::logout();
        }

        return redirect()->route('get_employee_dashboard');
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('get_employee_login');
    }

    public function getRegister()
    {
        $role = EmployeeRole::find(Auth::user()->employee_role_id)->role;
        if ($role != "admin") {
            return redirect()->back();
        }

        $roles = EmployeeRole::where('deleted', '=', '0')->get();

        return view('employee.register', ['roles' => $roles]);
    }

    public function postRegister(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required | max: 255',
            'last_name' => 'required | max: 255',
            'nationality' => 'required | max: 255',
            'national_number' => 'required | min: 10 | max: 10 | unique:employees',
            'email' => 'required | max: 255 | unique:employees',
            'address' => 'required | max: 255',
            'gender' => 'required | alpha | max:50',
            'employee_role_id' => 'required | numeric',
            'password' => 'required | max:255 | confirmed | min:8',
            'password_confirmation' => 'required | max:255 | min:8'
        ]);

        $employee = new Employee();
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->nationality = $request->nationality;
        $employee->national_number = $request->national_number;
        $employee->email = $request->email;
        $employee->address = $request->address;
        $employee->gender = $request->gender;
        $employee->profile_image_path = "uploads/img/profileImage/emptyprofile.jpg";
        $employee->employee_role_id = $request->employee_role_id;
        $employee->national_card_image_path = "";
        $employee->identity_certificate_image_path = "";
        $employee->password = bcrypt($request->password);
        $employee->save();

        return redirect()->route('get_employees_info');
    }
}
