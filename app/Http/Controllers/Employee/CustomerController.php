<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\AccountType;
use App\EmployeeRole;
use App\LoanType;
use App\Customer;
use App\Account;
use App\EmployeeAddCustomerLog;
use App\EmployeeWithdrawDepositAccountLog;

class CustomerController extends Controller
{
    public function postWithdrawFromCustomerAccount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'account_id' => 'required | numeric',
            'amount' => 'required | numeric'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        if ($request->amount < 0) {
            return response()->json(['fail' => 'amount is not correct'], 400);
        }

        $account = Account::find($request->account_id);
        if (is_null($account)) {
            return response()->json(['fail' => 'There is no account with this number'], 400);
        }
        if ($account->deleted == 1) {
            return response()->json(['fail' => 'Account deleted'], 400);
        }

        if ($request->amount > $account->balance) {
            return response()->json(['fail' => 'The balance is lower than amount'], 400);
        }

        $account->balance -= $request->amount;
        $account->save();

        $employeeWithdrawDepositAccountLog = new EmployeeWithdrawDepositAccountLog();
        $employeeWithdrawDepositAccountLog->employee_id = Auth::user()->id;
        $employeeWithdrawDepositAccountLog->account_id = $request->account_id;
        $employeeWithdrawDepositAccountLog->amount = -$request->amount;
        $employeeWithdrawDepositAccountLog->save();

        return response()->json('withdraw successfully', 200);
    }

    public function postDepositToCustomerAccount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'account_id' => 'required | numeric',
            'amount' => 'required | numeric'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        if ($request->amount < 0) {
            return response()->json(['fail' => 'amount is not correct'], 400);
        }

        $account = Account::find($request->account_id);
        if (is_null($account)) {
            return response()->json(['fail' => 'There is no account with this number'], 400);
        }
        if ($account->deleted == 1) {
            return response()->json(['fail' => 'Account deleted'], 400);
        }


        $account->balance += $request->amount;
        $account->save();

        $employeeWithdrawDepositAccountLog = new EmployeeWithdrawDepositAccountLog();
        $employeeWithdrawDepositAccountLog->employee_id = Auth::user()->id;
        $employeeWithdrawDepositAccountLog->account_id = $request->account_id;
        $employeeWithdrawDepositAccountLog->amount = $request->amount;
        $employeeWithdrawDepositAccountLog->save();

        return response()->json('deposit successfully', 200);
    }

    public function postAddCustomer(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required | max: 255',
            'last_name' => 'required | max: 255',
            'nationality' => 'required | max: 255',
            'national_number' => 'required | min: 10 | max: 10 | unique:employees',
            'email' => 'required | max: 255 | unique:employees',
            'address' => 'required | max: 255',
            'gender' => 'required | alpha | max:50',
        ]);

        $customer = new Customer();
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->national_number = $request->national_number;
        $customer->nationality = $request->nationality;
        $customer->email = $request->email;
        $customer->gender = $request->gender;
        $customer->address = $request->address;
        $customer->profile_image_path = "uploads/img/profileImage/emptyprofile.jpg";
        $customer->national_card_image_path = "";
        $customer->identity_certificate_image_path = "";
        $customer->save();

        $employeeAddCustomerLog = new EmployeeAddCustomerLog();
        $employeeAddCustomerLog->employee_id = Auth::user()->id;
        $employeeAddCustomerLog->customer_id = $customer->id;
        $employeeAddCustomerLog->save();

        return redirect()->route('get_customers_info');
    }

    public function getAddCustomer()
    {
        $employeeRoles = EmployeeRole::where('deleted', '=', '0')->get();
        $user = Auth::user();
        $userRole = EmployeeRole::find($user->employee_role_id);
        $accountTypes = AccountType::where('deleted', '=', '0')->get();
        $loanTypes = LoanType::where('deleted', '=', '0')->get();

        return view('employee.add_customer', ['user' => $user, 'loanTypes' => $loanTypes, 'userRole' => $userRole, 'employeeRoles' => $employeeRoles, 'accountTypes' => $accountTypes ]);
    }

    public function getCustomersInfo()
    {
        $employeeRoles = EmployeeRole::where('deleted', '=', '0')->get();
        $user = Auth::user();
        $userRole = EmployeeRole::find($user->employee_role_id);
        $accountTypes = AccountType::where('deleted', '=', '0')->get();
        $loanTypes = LoanType::where('deleted', '=', '0')->get();
        $customers = DB::table('customers')->
                     where('customers.deleted', '=', 0)->
                     select('customers.first_name', 'customers.last_name', 'customers.national_number', 'accounts.id as account_id', 'accounts.balance', 'account_types.name as account_type_name', 'accounts.deleted as account_deleted')->
                     leftJoin('accounts', 'customers.id', '=', 'accounts.customer_id')->join('account_types', 'account_types.id', '=', 'accounts.account_type_id')->get();

        return view('employee.customers_info', ['customers' => $customers, 'user' => $user, 'loanTypes' => $loanTypes, 'userRole' => $userRole, 'employeeRoles' => $employeeRoles, 'accountTypes' => $accountTypes ]);
    }
}
