<!DOCTYPE html>
<html>
<head>
    <title>Register</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">


    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <link rel="stylesheet" href="{{ URL::to('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/bootstrap-rtl.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/material-kit.css') }}">

    <link rel="stylesheet" href="{{ URL::to('assets/css/employee-login.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/helpers.css') }}">
    <script src="{{ URL::to('assets/js/jquery.min.js') }}"></script>
</head>
<body>

    <div class="container-fluid">
        <div class="row bg-dark">
            <div class="col-sm-4 col-sm-offset-4" align="center">
                <img src="{{ URL::to('assets/img/bg_logo.png') }}">
            </div>
        </div>
        <div>
            <div class="col-sm-8 col-sm-offset-2 middle shadow-depth-1" style="margin-bottom: 20px;">
                <div class="col-sm-6">
                    <h4 class="text-info" align="center">ثبت نام در سامانه</h4>
                    <hr class="style-two">
                    <form class="form-horizontal" action="{{ route('post_employee_register') }}" method="POST">
                        <div class="input-group ">
                            <span class="input-group-addon">
                                <i class="fa fa-user fa-2x"></i>
                            </span>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="firstName" placeholder="نام" name="first_name" value="{{ old('first_name') }}">
                                @if($errors->has('first_name'))
                                  <p class="small text-danger">{{ $errors->get('first_name')[0] }}</p>
                                @endif
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-user fa-2x"></i>
                            </span>
                            <div class="col-sm-8">
                                <input type="text" class="form-control inputstl" id="lastName" placeholder="نام خانوادگی" name="last_name" value="{{ old('last_name') }}">
                                @if($errors->has('last_name'))
                                  <p class="small text-danger">{{ $errors->get('last_name')[0] }}</p>
                                @endif
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-flag-o fa-2x" aria-hidden="true"></i>
                            </span>
                            <div class="col-sm-8">
                                <input type="text" class="form-control inputstl" id="nationality" placeholder="ملیت" name="nationality" value="{{ old('nationality') }}">
                                @if($errors->has('nationality'))
                                  <p class="small text-danger">{{ $errors->get('nationality')[0] }}</p>
                                @endif
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-id-card fa-2x" aria-hidden="true"></i>
                            </span>
                            <div class="col-sm-8">
                                <input type="text" class="form-control inputstl" id="nationalNum" placeholder="کد ملی" name="national_number" value="{{ old('national_number') }}">
                                @if($errors->has('national_number'))
                                  <p class="small text-danger">{{ $errors->get('national_number')[0] }}</p>
                                @endif
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-envelope fa-2x" aria-hidden="true"></i>
                            </span>
                            <div class="col-sm-8">
                                <input type="email" class="form-control inputstl" id="email" placeholder="ایمیل" name="email" value="{{ old('email') }}">
                                @if($errors->has('email'))
                                  <p class="small text-danger">{{ $errors->get('email')[0] }}</p>
                                @endif
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-address-card fa-2x" aria-hidden="true"></i>
                            </span>

                            <div class="col-sm-8">
                                <input class="form-control inputstl" id="address" type="text" placeholder="آدرس" name="address" value="{{ old('address') }}">
                                @if($errors->has('address'))
                                  <p class="small text-danger">{{ $errors->get('address')[0] }}</p>
                                @endif
                            </div>
                        </div>
                        <div class="form-group" style="padding-right:15%">
                            <div class="col-sm-4">
                                <select class="selectpicker" data-style="select-with-transition" title="جنسیت" data-size="2" name="gender">
                                    <option value="male">مذکر</option>
                                    <option value="female">مونث</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <select class="selectpicker" data-style="select-with-transition" title="نقش " data-size="7" name="employee_role_id">
                                    @foreach ($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->role }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-lock fa-2x"></i>
                            </span>
                            <div class="col-sm-8">
                                <input type="password" class="form-control inputstl" id="password" placeholder="رمز عبور" name="password">
                                @if($errors->has('password'))
                                  <p class="small text-danger">{{ $errors->get('password')[0] }}</p>
                                @endif
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-lock fa-2x"></i>
                            </span>
                            <div class="col-sm-8">
                                <input type="password" class="form-control inputstl" id="passwordConfirm" placeholder="تکرار رمز عبور" name="password_confirmation">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-1 col-sm-8">
                                <button type="submit" class="btn btn-sm btn-block btn-info">ثبت نام </button>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-6">
                    <h4 class="text-danger" align="center">نکات امنیتی</h4>
                    <hr class="style-two">
                    <ul class="s-icon">
                        <li> از صحت URL وارد شده درصفحه مرورگر خود اطمینان حاصل فرمایید. </li>
                        <li>رمز عبور خود را در فواصل زمانی کوتاه تغییر داده و هرگز آن را به سایرین ندهید.</li>
                        <li>همواره از رايانه‌ امن مجهز به ضدویروس به روز شده و دارای آخرین وصله‌های امنیتی استفاده نمایید.</li>
                        <li>بانک هرگز اطلاعات محرمانه شما را از طریق ایمیل یا پیامک درخواست نمی کند. در صورت رخداد این مورد،
                            ضمن عدم پاسخگویی به چنین درخواستهایی مراتب را سریعاً به بانک اطلاع دهید.</li>
                    </ul>
                </div>

            </div>
        </div>
    </div>


    <script src="{{ URL::to('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/material.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/material-kit.js') }}"></script>
    <script src="{{ URL::to('assets/js/bootstrap-select.js') }}"></script>
</body>
</html>
