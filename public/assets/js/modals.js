$(document).ready(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $("#addEmployeeRoleForm").submit(function(e) {
    e.preventDefault();
    var formData = $("#addEmployeeRoleForm").serialize();
    $.ajax({
        url: 'http://localhost:8000/employee/dashboard/add/employee/role',
        type: 'POST',
        dataType: 'JSON',
        data: formData
      })
      .done(function(data) {
        $("#addEmployeeRoleFormRoleError").text('');
        $('#add-role').modal('toggle');
        $("select[name='roleId']").append('<option value="' + data.roleId + '">' + $("input[name='role']").val() + '</option>');
        $("input[name='role']").val('');
      })
      .fail(function(data) {
        $("#addEmployeeRoleFormRoleError").text(data.responseJSON.role[0]);
      });
  });

  $("#deleteEmployeeRoleForm").submit(function(e) {
    e.preventDefault();

    var formData = $('#deleteEmployeeRoleForm').serialize();
    $.ajax({
        url: 'http://localhost:8000/employee/dashboard/delete/employee/role',
        type: 'POST',
        dataType: 'JSON',
        data: formData
      })
      .done(function(data) {
        $("select[name='roleId'] > option[value='" + formData.substring(7) + "']").remove();
        $("#delete-role").modal('toggle');
      })
      .fail(function(data) {});
  });

  $('#addAccountTypeForm').submit(function(e) {
    e.preventDefault();

    var formData = $('#addAccountTypeForm').serialize();
    $.ajax({
        url: 'http://localhost:8000/employee/dashboard/add/account/type',
        type: 'POST',
        dataType: 'JSON',
        data: formData
      })
      .done(function(data) {
        $("select[name='accountTypeId']").append('<option value="' + data.accountTypeId + '">' + $("input[name='accountTypeName']").val() + '</option>');
        $('#addAccountTypeFormNameError').text('');
        $('#addAccountTypeFormInterestError').text('');
        $("input[name='accountTypeName']").val('');
        $("input[name='accountTypeInterestRate']").val('');
        $('#add_acount_type').modal('toggle');
      })
      .fail(function(data) {
        if (data.responseJSON.accountTypeName) {
          $('#addAccountTypeFormNameError').text(data.responseJSON.accountTypeName[0]);
        } else {
          $('#addAccountTypeFormNameError').text('');
        }
        if (data.responseJSON.accountTypeInterestRate) {
          $('#addAccountTypeFormInterestError').text(data.responseJSON.accountTypeInterestRate[0]);
        } else {
          $('#addAccountTypeFormInterestError').text('');
        }
      });
  });

  $('#deleteAccountTypeForm').submit(function(e) {
    e.preventDefault();
    var formData = $('#deleteAccountTypeForm').serialize();

    $.ajax({
        url: 'http://localhost:8000/employee/dashboard/delete/account/type',
        type: 'POST',
        dataType: 'JSON',
        data: formData
      })
      .done(function(data) {
        $('#delete_acount_type').modal('toggle');
        $("select[name='accountTypeId'] > option[value='" + formData.substring(14) + "']").remove();
      })
      .fail();
  });

  $('#editAccountTypeForm').submit(function(e) {
    e.preventDefault();
    var formdata = $('#editAccountTypeForm').serialize();

    $.ajax({
        url: 'http://localhost:8000/employee/dashboard/edit/account/type',
        type: 'POST',
        datatype: 'JSON',
        data: formdata
      })
      .done(function(data) {
        $('#edit_acount_type').modal('toggle');
        $("input[name='accountTypeInterestRate']").val('');
      })
      .fail(function(data) {
        if (data.responseJSON.accountTypeInterestRate) {
          $('#editAccountTypeFormInterestError').text(data.responseJSON.accountTypeInterestRate[0]);
        } else {
          $('#editAccountTypeFormInterestError').text('');
        }
      });
  });


  $('#addLoanTypeForm').submit(function(e) {
    e.preventDefault();
    var formData = $('#addLoanTypeForm').serialize();

    $.ajax({
        url: 'http://localhost:8000/employee/dashboard/add/loan/type',
        type: 'POST',
        dataType: 'JSON',
        data: formData
      })
      .done(function(data) {
        $('#add_loan_type').modal('toggle');
        $("select[name='loanTypeId']").append('<option value="' + data.loanTypeId + '">' + $("input[name='loanTypeName']").val() + '</option>');
        $('input[name="loanTypeName"]').val('');
        $('input[name="amount"]').val('');
        $('input[name="interest"]').val('');
        $('input[name="number_of_payments"]').val('');
      })
      .fail(function(data) {

        if (data.responseJSON.loanTypeName) {
          $("#addLoanTypeFormNameError").text(data.responseJSON.loanTypeName[0]);
        } else {
          $("#addLoanTypeFormNameError").text('');
        }

        if (data.responsejson.amount) {
          $("#addLoanTypeFormAmountError").text(data.responseJSON.amount[0]);
        } else {
          $("#addLoanTypeFormAmountError").text('');
        }

        if (data.responseJSON.interest) {
          $("#addLoanTypeFormInterestError").text(data.responseJSON.interest[0]);
        } else {
          $("#addLoanTypeFormInterestError").text('');
        }

        if (data.responseJSON.number_of_payments) {
          $("#addLoanTypeFormNumberOfPaymentsError").text(data.responseJSON.number_of_payments[0]);
        } else {
          $("#addLoanTypeFormNumberOfPaymentsError").text('');
        }

      });
  });


  $('#deleteLoanTypeForm').submit(function(e) {
    e.preventDefault();
    var formData = $('#deleteLoanTypeForm').serialize();

    $.ajax({
        url: 'http://localhost:8000/employee/dashboard/delete/loan/type',
        type: 'POST',
        dataType: 'JSON',
        data: formData
      })
      .done(function(data) {
        $('#delete_loan_type').modal('toggle');
        $("select[name='loanTypeId'] > option[value='" + formData.substring(11) + "']").remove();

      })
      .fail(function(data) {});
  });


  $('#editLoanTypeForm').submit(function(e) {
    e.preventDefault();
    var formdata = $('#editLoanTypeForm').serialize();

    $.ajax({
        url: 'http://localhost:8000/employee/dashboard/edit/loan/type',
        type: 'POST',
        datatype: 'JSON',
        data: formdata
      })
      .done(function(data) {
        $('#edit_loan_type').modal('toggle');
        $("input[name='amount']").val('');
        $("input[name='interest']").val('');
        $("input[name='number_of_payments']").val('');
      })
      .fail(function(data) {

        if (data.responseJSON.amount) {
          $("#editLoanTypeFormAmountError").text(data.responseJSON.amount[0]);
        } else {
          $("#editLoanTypeFormAmountError").text('');
        }

        if (data.responseJSON.interest) {
          $("#editLoanTypeFormInterestError").text(data.responseJSON.interest[0]);
        } else {
          $("#editLoantypeFormInterestError").text('');
        }

        if (data.responseJSON.number_of_payments) {
          $("#editLoanTypeFormNumberOfPaymentsError").text(data.responseJSON.number_of_payments[0]);
        } else {
          $("#editLoanTypeFormNumberOfPaymentsError").text('');
        }

      });
  });


  $('#depositToCustomerAccountForm').submit(function(e) {
    e.preventDefault();
    var formData = $('#depositToCustomerAccountForm').serialize();

    $.ajax({
        url: 'http://localhost:8000/employee/dashboard/deposit/to/customer/account',
        type: 'POST',
        dataType: 'JSON',
        data: formData
      })
      .done(function(data) {
        $('#add-money').modal('toggle');
        $("input[name='account_id']").val('');
        $("input[name='amount']").val('');

      })
      .fail(function(data) {
        if (data.responseJSON.account_id) {
          $("#depositToCustomerAccountFormAccountIdError").text(data.responseJSON.account_id[0]);
        } else {
          $("#depositToCustomerAccountFormAccountIdError").text('');
        }

        if (data.responseJSON.amount) {
          $("#depositToCustomerAccountFormAmountError").text(data.responseJSON.amount[0]);
        } else if (data.responseJSON.fail) {
          $("#depositToCustomerAccountFormAmountError").text(data.responseJSON.fail);
        } else {
          $("#depositToCustomerAccountFormAmountError").text('');
        }

      });
  });

  $('#withdrawFromCustomerAccountForm').submit(function(e) {
    e.preventDefault();
    var formData = $('#withdrawFromCustomerAccountForm').serialize();

    $.ajax({
        url: 'http://localhost:8000/employee/dashboard/withdraw/from/customer/account',
        type: 'POST',
        dataType: 'JSON',
        data: formData
      })
      .done(function(data) {
        $('#draw').modal('toggle');
        $("input[name='account_id']").val('');
        $("input[name='amount']").val('');

      })
      .fail(function(data) {
        if (data.responseJSON.account_id) {
          $("#withdrawFromCustomerAccountFormAccountIdError").text(data.responseJSON.account_id[0]);
        } else {
          $("#withdrawFromCustomerAccountFormAccountIdError").text('');
        }

        if (data.responseJSON.amount) {
          $("#withdrawFromCustomerAccountFormAmountError").text(data.responseJSON.amount[0]);
        } else if (data.responseJSON.fail) {
          $("#withdrawFromCustomerAccountFormAmountError").text(data.responseJSON.fail);
        } else {
          $("#withdrawFromCustomerAccountFormAmountError").text('');
        }

      });
  });


  $('#deleteAccountForm').submit(function(e) {
    e.preventDefault();
    var formData = $('#deleteAccountForm').serialize();

    $.ajax({
        url: 'http://localhost:8000/employee/dashboard/delete/account',
        type: 'POST',
        dataType: 'JSON',
        data: formData
      })
      .done(function(data) {
        $('#delete_acount').modal('toggle');
        $("input[name='account_id']").val('');

      })
      .fail(function(data) {
        if (data.responseJSON.account_id) {
          $("#deleteAccountFormError").text(data.responseJSON.account_id[0]);
        } else if (data.responseJSON.fail) {
          $("#deleteAccountFormError").text(data.responseJSON.fail);
        } else {
          $("#deleteAccountFormError").text('');
        }
      });
  });

  $('#giveCheckForm').submit(function(e) {
    e.preventDefault();
    var formData = $('#giveCheckForm').serialize();

    $.ajax({
        url: 'http://localhost:8000/employee/dashboard/give/check',
        type: 'POST',
        dataType: 'JSON',
        data: formData
      })
      .done(function(data) {
        $('#assign_check').modal('toggle');
        $("input[name='account_id']").val('');
        $("input[name='counts']").val('');

      })
      .fail(function(data) {
        if (data.responseJSON.account_id) {
          $("#giveCheckFormAccountIdError").text(data.responseJSON.account_id[0]);
        } else {
          $("#giveCheckFormAccountIdError").text(data.responseJSON.account_id[0]);
        }
        if (data.responseJSON.counts) {
          $("#giveCheckFormCountsError").text(data.responseJSON.counts[0]);
        } else {
          $("#giveCheckFormCountsError").text(data.responseJSON.counts[0]);
        }
      });
  });
});
