<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\AccountType;
use App\EmployeeRole;
use App\LoanType;
use App\Account;
use App\Loan;
use App\EmployeeLendLog;
use App\EmployeeLoanPaymentLog;

class LoanController extends Controller
{

    public function postPayLoan(Request $request){
        $this->validate($request, [
            'loan_id' => 'required | numeric',
            'amount' => 'required | numeric'
        ]);

        $loan = Loan::find($request->loan_id);
        if(is_null($loan)){
            return back()->withErrors(['loan_id' => "There is no loan with this loan id"]);
        }

        if($loan->deleted == 1){
            return back()->withErrors(['loan_id' => 'This loan was deleted']);
        }

        if($request->amount < 0){
            return back()->withErrors(['amount' => 'The amount is not correct']);
        }

        $loanType = LoanType::find($loan->loan_type_id);
        if($loan->payed_amount >= ( $loanType->amount + ($loanType->amount * $loanType->interest) / 100) ){
            return back()->withErrors(['loan_id' => 'This loan was payed completely']);
        }

        $loan->payed_amount += $request->amount;
        $loan->save();

        $employeeLoanPaymentLog = new EmployeeLoanPaymentLog();
        $employeeLoanPaymentLog->employee_id = Auth::user()->id;
        $employeeLoanPaymentLog->loan_id = $loan->id;
        $employeeLoanPaymentLog->amount = $request->amount;
        $employeeLoanPaymentLog->save();

        return redirect()->route('get_customers_loans_info');
    }

    public function getCustomersLoansInfo(){
        $employeeRoles = EmployeeRole::where('deleted', '=', '0')->get();
        $user = Auth::user();
        $userRole = EmployeeRole::find($user->employee_role_id);
        $accountTypes = AccountType::where('deleted', '=', '0')->get();
        $loanTypes = LoanType::where('deleted', '=', '0')->get();
        $loansInfo = DB::table('customers')
                        ->select('customers.first_name', 'customers.last_name', 'loans.id as loan_id', 'loans.payed_amount', 'loan_types.name as loan_type_name')
                        ->where('customers.deleted', '=', '0')
                        ->where('accounts.deleted', '=', '0')
                        ->where('loans.deleted', '=', '0')
                        ->join('accounts', 'accounts.customer_id', '=', 'customers.id')
                        ->join('loans', 'loans.account_id', '=', 'accounts.id')
                        ->join('loan_types', 'loans.loan_type_id', '=', 'loan_types.id')
                        ->get();

        return view('employee.customers_loans_info', ['loansInfo' => $loansInfo, 'user' => $user, 'loanTypes' => $loanTypes, 'userRole' => $userRole, 'employeeRoles' => $employeeRoles, 'accountTypes' => $accountTypes ]);
    }

    public function postGiveLoan(Request $request)
    {
        $this->validate($request, [
            'account_id' => 'required | numeric',
            'loan_type_id' => 'required | numeric'
        ]);

        $account = Account::find($request->account_id);
        if (is_null($account)) {
            return back()->withErrors(['account_id' => 'There is no account with this number']);
        }

        if($account->deleted == 1){
            return back()->withErrors(['account_id' => 'This is a deleted account']);
        }

        //TODO check for account type

        $loan = new Loan();
        $loan->account_id = $request->account_id;
        $loan->loan_type_id = $request->loan_type_id;
        $loan->payed_amount = 0;
        $loan->save();

        $employeeLendLog = new EmployeeLendLog();
        $employeeLendLog->employee_id = Auth::user()->id;
        $employeeLendLog->loan_id = $loan->id;
        $employeeLendLog->save();

        return redirect()->route('get_customers_loans_info');

    }
    public function postEditLoanType(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'amount' => 'required | numeric',
          'interest' => 'required | numeric',
          'number_of_payments' => 'required | numeric'
      ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $loanType = LoanType::find($request->loanTypeId);
        $loanType->amount = $request->amount;
        $loanType->interest = $request->interest;
        $loanType->number_of_payments = $request->number_of_payments;
        $loanType->save();

        return response()->json('edited successfully', 200);
    }

    public function postDeleteLoanType(Request $request)
    {
        if (!EmployeeController::isAdmin()) {
            return back();
        }


        $validator = Validator::make($request->all(), [
            'LoanTypeId' => 'required'
        ]);

        $loanType = LoanType::find($request->loanTypeId);
        $loanType->deleted = 1;
        $loanType->save();

        return response()->json("deleted successfully", 200);
    }

    public function postAddLoanType(Request $request)
    {
        if (!EmployeeController::isAdmin()) {
            return back();
        }

        $validator = Validator::make($request->all(), [
          'loanTypeName' => ['required' , 'max:250' , 'unique:loan_types,name'],
          'amount' => 'required | numeric',
          'interest' => 'required | numeric',
          'number_of_payments' => 'required | numeric'
      ]);

        if ($validator->messages()->has('loanTypeName') && $validator->messages()->get('loanTypeName')[0] == "The loan type name has already been taken."
        && !$validator->messages()->has('amount') && !$validator->messages()->has('interest') && !$validator->messages()->has('number_of_payments')) {
            $loanType = LoanType::where('name', '=', $request->loanTypeName)->first();
            if ($loanType->deleted == 1) {
                $loanType->deleted = 0;
                $loanType->amount = $request->amount;
                $loanType->interest = $request->interest;
                $loanType->number_of_payments = $request->number_of_payments;
                $loanType->save();
            } else {
                return response()->json($validator->messages(), 400);
            }
        } elseif ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        } else {
            $loanType = new LoanType();
            $loanType->name = $request->loanTypeName;
            $loanType->amount = $request->amount;
            $loanType->interest = $request->interest;
            $loanType->number_of_payments = $request->number_of_payments;
            $loanType->save();
        }
        return response()->json(['loanTypeId' => $loanType->id], 200);
    }

    public function getGiveLoan()
    {
        $employeeRoles = EmployeeRole::where('deleted', '=', '0')->get();
        $user = Auth::user();
        $userRole = EmployeeRole::find($user->employee_role_id);
        $accountTypes = AccountType::where('deleted', '=', '0')->get();
        $loanTypes = LoanType::where('deleted', '=', '0')->get();

        return view('employee.lend', ['user' => $user, 'loanTypes' => $loanTypes, 'userRole' => $userRole, 'employeeRoles' => $employeeRoles, 'accountTypes' => $accountTypes ]);
    }

    public function getPayLoan()
    {
        $employeeRoles = EmployeeRole::where('deleted', '=', '0')->get();
        $user = Auth::user();
        $userRole = EmployeeRole::find($user->employee_role_id);
        $accountTypes = AccountType::where('deleted', '=', '0')->get();
        $loanTypes = LoanType::where('deleted', '=', '0')->get();

        return view('employee.pay_loan', ['user' => $user, 'loanTypes' => $loanTypes, 'userRole' => $userRole, 'employeeRoles' => $employeeRoles, 'accountTypes' => $accountTypes ]);
    }
}
