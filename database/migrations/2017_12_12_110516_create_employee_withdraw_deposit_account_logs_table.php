<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeWithdrawDepositAccountLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('employee_withdraw_deposit_account_logs', function (Blueprint $table) {
            $table->unsignedInteger('employee_id');
            $table->unsignedInteger('account_id');
            $table->double('amount');
            $table->timestamps();
        });


        Schema::table('employee_withdraw_deposit_account_logs', function ($table) {
          $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
          $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_withdraw_deposit_account_logs');
    }
}
